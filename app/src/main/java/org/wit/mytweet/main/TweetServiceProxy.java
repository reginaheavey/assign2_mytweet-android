package org.wit.mytweet.main;

import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.Tweeter;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;

public interface TweetServiceProxy
{

  @GET("/api/tweeters/authenticate/{email}/{password}")
  Call<Tweeter> authenticate(@Path("email") String email, @Path("password") String password);

  @GET("/api/tweeters/{id}/tweets")
  Call<List<Tweet>> getTweetsByUserId(@Path("id") String id);

  //Tweeter

  @GET("/api/tweeters")
  Call<List<Tweeter>> getAllTweeters();

  @GET("/api/tweeters/{id}")
  Call<Tweeter> getTweeter(@Path("id") String id);

  @POST("/api/tweeters")
  Call<Tweeter> createTweeter(@Body Tweeter tweeter);

  @DELETE("/api/tweeters/{id}")
  Call<Tweeter> deleteTweeter(@Path("id") String id);

  @DELETE("/api/tweeters")
  Call<String> deleteAllTweeters();


  //Tweets

  // Get all Tweets
  @GET("/api/tweets")
  Call<List<Tweet>> getAllTweets();

  // Delete all tweets
  @DELETE("/api/tweets")
  Call<String> deleteAllTweets();

  @GET("/api/tweeters/{id}/tweets")
  Call<List<Tweet>> getTweets(@Path("id") String id);

  @GET("/api/tweeters/{id}/tweets/{tweetId}")
  Call<Tweet> getTweet(@Path("id") String id, @Path("tweetId") String tweetId);

  @POST("/api/tweeters/{id}/tweets")
  Call<Tweet> createTweet(@Path("id") String id, @Body Tweet tweet);

  @DELETE("/api/tweeters/{id}/tweets/{tweetId}")
  Call<String> deleteTweet(@Path("id") String id, @Path("tweetId") String tweetId);
}

