package org.wit.mytweet.activities;

import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.R;
import org.wit.mytweet.models.Portfolio;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.Tweeter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static org.wit.helpers.IntentHelper.startActivityWithData;


public class TweetListActivity extends Activity implements AdapterView.OnItemClickListener, Callback<List<Tweet>>
{
    private ListView listView;
    private Portfolio portfolio;
    private TweetAdapter adapter;
    private Tweeter tweeter;
    private MyTweetApp app;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tweetlist);

        listView = (ListView) findViewById(R.id.tweetList);
        listView.setOnItemClickListener(this);

        app = (MyTweetApp) getApplication();
        portfolio = app.portfolio;

        tweeter = app.logged_in_tweeter;

        adapter = new TweetAdapter(this, portfolio.tweets);
        listView.setAdapter(adapter);

        refreshTweetList();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Tweet tweet = adapter.getItem(position);
        startActivityWithData(this, TweetActivity.class, "TWEET_ID", tweet.id);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.tweetlist, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case R.id.menu_item_new_tweet:
                startActivity(new Intent(this, TweetActivity.class));
                break;

            case R.id.menuClear:
                adapter.clear();
                deleteAllRemoteTweets();
                break;

            case R.id.action_refresh:
                adapter.clear();
                refreshTweetList();
                break;

            case R.id.action_logout:
                startActivity(new Intent(this, Welcome.class));
                app.logged_in_tweeter = null;
                finish();
                break;
        }
        return true;
    }

    // new method to handle Call<String>
    private void deleteAllRemoteTweets()
    {
        Call<String> call = app.tweetService.deleteAllTweets();
        call.enqueue(new Callback<String>()
        {

            @Override
            public void onResponse(Response<String> response, Retrofit retrofit)
            {
                Toast.makeText(TweetListActivity.this, "All Tweets deleted: " + response.body(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Throwable t)
            {
                Toast.makeText(TweetListActivity.this, "Failed to delete all tweets", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void refreshTweetList()
    {
        Call<List<Tweet>> call = app.tweetService.getTweetsByUserId(app.logged_in_tweeter.id);
        call.enqueue(this);
    }

    @Override
    public void onResponse(Response<List<Tweet>> response, Retrofit retrofit)
    {
        List<Tweet> list = response.body();
        app.portfolio.tweets.clear();
        app.portfolio.tweets.addAll(list);
        adapter.notifyDataSetChanged();
        Toast.makeText(TweetListActivity.this, "Retrieved " + list.size() + " tweets", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailure(Throwable t)
    {
        Toast.makeText(TweetListActivity.this, "Failed to retrieve tweet list", Toast.LENGTH_LONG).show();
    }
}


//--------------------------------------------------------------------------------------------

// Adapter Class
class TweetAdapter extends ArrayAdapter<Tweet>
{
    private Context context;

    public TweetAdapter(Context context, ArrayList<Tweet> tweets) {
        super(context, 0, tweets);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null)
        {
            convertView = inflater.inflate(R.layout.list_item_tweet, null);
        }
        Tweet twe = getItem(position);

        TextView tweetText = (TextView) convertView.findViewById(R.id.tweet_list_item_tweetText);
        tweetText.setText(twe.tweetText);

        TextView tweetDateTime = (TextView) convertView.findViewById(R.id.tweet_list_item_dateTextView);
        tweetDateTime.setText(twe.getDateString());

        return convertView;
    }
}
