package org.wit.mytweet.activities;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Portfolio;
import org.wit.mytweet.models.Tweet;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static org.wit.helpers.IntentHelper.startActivityWithData;

public class Welcome extends Activity implements AdapterView.OnItemClickListener, Callback<List<Tweet>>
{

    private ListView listView;
    private Portfolio portfolio;
    private TweetAdapter adapter;
    private MyTweetApp app;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);

        listView = (ListView) findViewById(R.id.tweetList);
        listView.setOnItemClickListener(this);

        app = (MyTweetApp) getApplication();
        portfolio = app.portfolio;

        adapter = new TweetAdapter(this, portfolio.tweets);
        listView.setAdapter(adapter);

        refreshTweetList();
    }

    private void refreshTweetList()
    {
        Call<List<Tweet>> call = app.tweetService.getAllTweets();
        call.enqueue(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Tweet tweet = adapter.getItem(position);
        startActivityWithData(this, TweetActivity.class, "TWEET_ID", tweet.id);
    }

    @Override
    public void onResponse(Response<List<Tweet>> response, Retrofit retrofit)
    {
        List<Tweet> list = response.body();
        app.portfolio.tweets.clear();
        app.portfolio.tweets.addAll(list);
        adapter.notifyDataSetChanged();
        Toast.makeText(Welcome.this, "Retrieved " + list.size() + " tweets", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailure(Throwable t)
    {
        Toast.makeText(Welcome.this, "Failed to retrieve tweet list", Toast.LENGTH_LONG).show();
    }

    public void loginPressed(View view)
    {
        startActivity(new Intent(this, Login.class));
    }

    public void signupPressed(View view)
    {
        startActivity(new Intent(this, Signup.class));
    }
}

