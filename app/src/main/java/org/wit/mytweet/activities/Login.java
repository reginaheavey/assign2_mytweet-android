package org.wit.mytweet.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.wit.mytweet.R;
import org.wit.mytweet.app.MyTweetApp;
import org.wit.mytweet.models.Portfolio;
import org.wit.mytweet.models.Tweet;
import org.wit.mytweet.models.Tweeter;

import java.net.HttpURLConnection;
import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

import static org.wit.helpers.IntentHelper.startActivityWithData;


public class Login extends Activity implements AdapterView.OnItemClickListener, Callback<List<Tweet>>
{

    private Portfolio portfolio;
    private TweetAdapter adapter;
    private MyTweetApp app;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        app = (MyTweetApp) getApplication();
        portfolio = app.portfolio;

        adapter = new TweetAdapter(this, portfolio.tweets);
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Tweet tweet = adapter.getItem(position);
        startActivityWithData(this, TweetActivity.class, "TWEET_ID", tweet.id);
    }


    public void signinPressed(View view)
    {
        final MyTweetApp app = (MyTweetApp) getApplication();

        TextView email = (TextView) findViewById(R.id.loginEmail);
        TextView password = (TextView) findViewById(R.id.loginPassword);

        Call<Tweeter> call = app.tweetService.authenticate(email.getText().toString(), password.getText().toString());
        call.enqueue(new Callback<Tweeter>()
        {

            @Override
            public void onResponse(Response<Tweeter> response, Retrofit retrofit)
            {
                if (response.code() == HttpURLConnection.HTTP_OK)
                {
                    app.logged_in_tweeter = response.body();
                    startActivity(new Intent(Login.this, TweetActivity.class));
                } else
                {
                    Toast toast = Toast.makeText(Login.this, "Invalid Credentials", Toast.LENGTH_SHORT);
                    toast.show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("onFailure", t.getMessage());
            }
        });

    }

    @Override
    public void onResponse(Response<List<Tweet>> response, Retrofit retrofit)
    {
        List<Tweet> list = response.body();
        app.portfolio.tweets.clear();
        app.portfolio.tweets.addAll(list);
        adapter.notifyDataSetChanged();
        Toast.makeText(Login.this, "Retrieved " + list.size() + " tweets", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFailure(Throwable t)
    {
        Toast.makeText(Login.this, "Failed to retrieve tweet list", Toast.LENGTH_LONG).show();
    }
}