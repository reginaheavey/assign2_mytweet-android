package org.wit.mytweet.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.wit.mytweet.app.MyTweetApp;

import org.wit.mytweet.R;
import org.wit.mytweet.models.Tweeter;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class Signup extends AppCompatActivity implements Callback<Tweeter>
{
    private MyTweetApp app;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        app = (MyTweetApp) getApplication();
    }


    public void registerPressed(View view)
    {
        TextView firstName = (TextView) findViewById(R.id.firstName);
        TextView lastName = (TextView) findViewById(R.id.lastName);
        TextView email = (TextView) findViewById(R.id.Email);
        TextView password = (TextView) findViewById(R.id.Password);

        Tweeter tweeter = new Tweeter(firstName.getText().toString(),
                lastName.getText().toString(),
                email.getText().toString(),
                password.getText().toString());

        MyTweetApp app = (MyTweetApp) getApplication();

        Call<Tweeter> call = app.tweetService.createTweeter(tweeter);
        call.enqueue(this);
    }


    @Override
    public void onResponse(Response<Tweeter> response, Retrofit retrofit)
    {
        app.tweeters.add(response.body());
        startActivity(new Intent(this, Welcome.class));
    }

    @Override
    public void onFailure(Throwable t)
    {
        Toast toast = Toast.makeText(this, "myTweet Service Unavailable. Try again later", Toast.LENGTH_LONG);
        toast.show();
        startActivity(new Intent(this, Welcome.class));
    }
}
