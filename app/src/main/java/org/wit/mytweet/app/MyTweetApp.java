package org.wit.mytweet.app;

import org.wit.mytweet.main.TweetServiceProxy;

import java.util.ArrayList;
import java.util.List;

import android.app.Application;

import org.wit.mytweet.models.Portfolio;
import org.wit.mytweet.models.PortfolioSerializer;
import org.wit.mytweet.models.Tweeter;

import static org.wit.helpers.LogHelpers.info;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class MyTweetApp extends Application
{
    public Portfolio portfolio;
    private static final String FILENAME = "portfolio.json";

    public String service_url = "http://10.0.3.2:9000"; //Genymotion
    public TweetServiceProxy tweetService;
    public Tweeter logged_in_tweeter;
    public List<Tweeter> tweetersList;
    public List<Tweeter> tweeters = new ArrayList<Tweeter>();

    @Override
    public void onCreate()
    {
        info(this, "on create");
        super.onCreate();
        PortfolioSerializer serializer = new PortfolioSerializer(this, FILENAME);
        portfolio = new Portfolio(serializer);
        info(this, "mytweetapp after serializer");
        Gson gson = new GsonBuilder().create();
        info(this, "gson");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(service_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        // As soon as app launches we have link to service & can start using it.
        tweetService = retrofit.create(TweetServiceProxy.class);

        info(this, "My Tweet app launched");
    }

    //Android Studio 2 step 7 has return as Tweeter
    public boolean validUser(String email, String password)
    {
        for (Tweeter tweeter : tweeters)
        {
            if (tweeter.email.equals(email) && tweeter.password.equals(password))
            {
                logged_in_tweeter = tweeter;
                return true;
            }
        }
        return false;
    }
}